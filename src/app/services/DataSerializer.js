import _forEach from 'lodash/collection/forEach';
import _sortBy from 'lodash/collection/sortBy';

import TreeNode from './../classes/TreeNode';

export default class DataSerializer {

	/**
	 * Transforms initial data (preorder tree traversal) to format required by tree drawer (d3.js)
	 * Initial data example:
	 *
	 * [
     * 	{ name: "Cars", left: 1, right: 18 },
	 * 	{ name: "Fast", left: 2, right: 11 },
	 * 	...........
	 * 	{ name: "Polonez", left: 15, right: 16 }
	 * ]
	 * 
	 * Result format:
	 *
	 * [
		{
			"name": "Cars",
		    "children": [
		      	{
		        	"name": "Fast",
		        	"children": [
		          		.....
		        	]
		      	}
		    ]
		]
	 * 
	 * @param  {object} data 	Initial data
	 * @return {object}      	Result data
	 */
	processTraversalMethod(data) {
		var items = [];
		var ordered = _sortBy(data, "left");

		if (ordered[0] && ordered[0].left !== 1) {
			throw new Error("No root element was provided");
		}

		var rootNode = new TreeNode(ordered[0]);

		// Push root element to the list
		items.push(ordered[0]);

		_forEach(ordered, item => {
			// Find parent
			var parentIndex = 0;
			items[parentIndex].children = item;
		});

		return items;
	}

}